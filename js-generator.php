<?php

header('Content-Type: application/javascript');

require_once 'libs/js-generator.class.php';

$scripts = [
	'lang' => "$('header select option[value=\"@{lang}\"]').attr('selected', 'true').trigger('change');"
];

$js = (new JSGenerator)
	->setScripts($scripts)
	->generate($_GET)
	->get();

echo($js);
