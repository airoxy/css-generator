<?php

header('Content-Type: text/css');

require_once 'libs/css-generator.class.php';

$selectors = [
	'menutype' => '.menu_list_group .list-group-item.active',
	'menugroup' => 'a.list-group-item, button.list-group-item',
	'menuofdaybutton' => '.bg-brown',
	'menusubgroup' => '.cl-brown',
	'menulist' => '.search-result-item',
	'menulistcomment' => '.ng-binding',
	'bookbutton' => 'button.btn.bg-green',

	'bgcolor' => ['body', 'background-color'],
	'showphoto' => ['.search-result-item img', 'display', ['no' => 'none', 'yes' => 'block']]
];

$css = (new CSSGenerator)
	->setSelectors($selectors)
	->generate($_GET)
	->getSelectors()
	->get();

echo($css);

