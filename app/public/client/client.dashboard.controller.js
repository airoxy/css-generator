/**
 * Created by yonatom on 8/31/16.
 */

(function () {
    'use strict';

    angular
        .module('app.client')
        .config(function(tagsInputConfigProvider) {
            tagsInputConfigProvider.setActiveInterpolation('tagsInput', { placeholder: true });
        })
    .controller('ClientDashboardController', ClientDashboardController);
    /*@ngNoInject*/
    function ClientDashboardController($rootScope,$scope,$timeout,ClientService, $state) {
        var vm = this;
        $rootScope.currentState = "dashboard";
        vm.friendCircle = [];
        vm.otherCircles = [];
        vm.friendRequests = [];
        vm.loadingFriendRequests = false;
        vm.sentFriendRequests = [];
        vm.friends = [];
        vm.connectSuccess = "";
        vm.respondToFriendRequestStatus = "";
        vm.doFade = false;
        vm.orders = [];
        vm.ordersDetail = [];
        vm.loading = false;
        vm.loadingBookingHistory = false;
        vm.loadingSaveChanges = false;
        vm.isOrderEmpty = false;
        vm.isOrderDetailEmpty =false;
        vm.currentPage = 1;
        vm.itemsPerPage = 5;
        vm.totalItems = 0;
        vm.statusNames = ['CLIENT.ORDERED', 'CLIENT.SENT', 'CLIENT.CONFIRMED', 'CLIENT.CANCELED', 'CLIENT.FINALIZED', 'CLIENT.NEW'];
        vm.sortStatus = {
            "status": {
                "new": 0,
                "sent": 0,
                "confirmed": 0,
                "canceled": 1,
                "finalized": 0,
                "all": 0,
            }
        };

        vm.placeholder = 'CLIENT.ENTER FRIEND NAME';

        vm.getFriends = getFriends;
        vm.getFriendRequests = getFriendRequests;
        vm.getFriendCircle = getFriendCircle;
        vm.getOtherCircles = getOtherCircles;
        vm.respondToFriendRequest = respondToFriendRequest;
        vm.addFriends = addFriends;
        vm.getOrders = getOrders;
        vm.changeOrderDetailStatus = changeOrderDetailStatus;
        vm.changeOrderStatus = changeOrderStatus;
        vm.saveChanges = saveChanges;
        vm.respond = respond;
        vm.loading = false;
        vm.gotoRestaurantDetail = gotoRestaurantDetail;

        getFriendCircle();
        getOtherCircles();
        getFriendRequests();
        getSentFriendRequests();
        getOrders();

        function getFriends(query){
            debugger;
            return ClientService.getFriends(query).then(function(response){
                debugger;
                return response.data
            });
        }

        function getFriendCircle(){
            debugger;
            ClientService.getFriendCircle().then(function(response){
                debugger;
                vm.friendCircle = response.data;
            },function(error){

            });
        }
        function getOtherCircles(){
            debugger;
            ClientService.getOtherCircles().then(function(response){
                debugger;
                vm.otherCircles = response.data;
            },function(error){

            });
        }

        function respondToFriendRequest(response,friendId){
            debugger;
            var response = {
                "response":{
                    "response":response,
                    "ID_grouped_client": friendId    
                }

            };
            ClientService.respondToFriendRequest(response).then(function(respons){
                debugger;
                var approved = response.response.response;
                vm.respondToFriendRequestStatus = approved == 'Y' ? "CLIENT.Accepted friend request!" : (approved == 'D') ? "CLIENT.Friend request declined!" : (approved == 'R') ? "CLIENT.User blocked successfully!" : "";
                vm.friends = [];
                $timeout(function(){
                    vm.doFade = true;
                    vm.respondToFriendRequestStatus = "";
                }, 2500);
                getFriendRequests();
                getFriendCircle();
                getOtherCircles();
            },function(error){
                debugger;
            });
        }
        function respond(response, friendId){
            var response = {
                "response":{
                    "response":response,
                    "ID_grouped_client": friendId
                }

            };
            ClientService.respondToFriendRequest(response).then(function(response){
                debugger;
                getFriendCircle();
                getOtherCircles();
            }, function(error){
                debugger;
            });
        }

        function getFriendRequests(){
            vm.loadingFriendRequests = true;
            debugger;
            ClientService.getFriendRequests().then(function(response){
                debugger;
                vm.friendRequests = response.data;
                vm.loadingFriendRequests = false;
            },function(error){
                vm.loadingFriendRequests = false;
                debugger;
            });
        }
        function addFriends(isValid){
            debugger;
            if (isValid){
                var friends = {"friends":vm.friends};

                ClientService.addFriends(friends).then(function(response){
                    debugger;
                    vm.connectSuccess = "CLIENT.Request sent successfully!";
                    vm.friends = [];
                    $timeout(function(){
                        vm.doFade = true;
                        vm.connectSuccess = "";
                    }, 2500);
                    getSentFriendRequests();
                },function(error) {
                    debugger;
                });
            }

        }
        function getSentFriendRequests(){
            debugger;
            ClientService.getSentFriendRequests().then(function(response){
                debugger;
                vm.sentFriendRequests = response.data;
            },function(error){
                debugger;
            });
        }

        function getOrders() {
            debugger;
            vm.loading = true;
            ClientService.getOrders(vm.currentPage).then(function (response) {
                debugger;
                vm.orders = response.data;
                vm.totalItems = response.meta.pagination.total;
                vm.itemsPerPage = response.meta.pagination.per_page;

                if (vm.orders.length == 0) {
                    vm.isOrderEmpty = true;
                }
                vm.loading = false;
                // arrangeOrdersDetail();
                debugger;
            }, function (error) {
                debugger;
                vm.loading = false;
            })
        }

        function changeOrderDetailStatus(newStatus, order, index) {
            debugger;
            if (newStatus == 6)
                $rootScope.$broadcast('orders-detail-changed');
            order.orders_detail.data[index].status = newStatus;
            debugger;
        }

        function changeOrderStatus(newStatus, order) {
            debugger;
            vm.loading = true;
            order.status = newStatus;
            var n_order = {
                "order": order,
                'lang': 'cz'

            };
            ClientService.updateOrder(n_order).then(function (response) {
                debugger;
                $rootScope.$broadcast('orders-detail-changed');
                vm.getOrders();
                vm.loading = false;
            }, function (error) {
                debugger;
                vm.loading = false;
            })
        }
        function saveChanges(order, index) {
            debugger;
            vm.loadingSaveChanges = true;
            var n_order_detail = {
                "orders_detail": order.orders_detail.data,
                "save" : true
            };
            ClientService.updateOrderDetails(n_order_detail, true).then(function (response) {
                debugger;
                vm.loadingSaveChanges = false;
                $('#orderDetail' + index).modal('hide');
                $rootScope.$broadcast('orders-detail-changed');
                getOrders();
            }, function (error) {
                debugger;
                vm.loadingSaveChanges = false;
                $('#orderDetail' + index).modal('hide');
            })
        }

        function gotoRestaurantDetail(order, index) {
            debugger;
            var id = '#orderDetail'+ index;
            $(id).modal('hide');
            debugger;
            $state.go('main.restaurant_detail', {'restaurantId': order.ID_restaurant});
        }

        //Pop over functions
        var trigger = $('button');

        function showTip() {
            if (! $('#tip').is(':visible')) {
                trigger.click();
            }
        }

        function hideTip() {
            if ($('#tip').is(':visible')) {
                trigger.click();
            }
        }

        trigger.mouseenter(showTip);

        $(document).on('mouseleave', '#tip', hideTip);
        //End pop over functions


    }

})();