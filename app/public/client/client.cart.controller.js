/**
 * Created by Thomas on 10/14/2016.
 */

(function () {
    'use strict';

    angular
        .module('app.client')
        .controller('ClientCartController', ClientCartController);
    /*@ngNoInject*/
    function ClientCartController($state, $rootScope,$scope,$timeout,ClientCartService,ClientService, uiDatetimePickerConfig, $translate, moment, appConstant) {
        var vm = this;
        vm.show = true;
        debugger;
        $rootScope.currentState = 'cart';
        vm.getOrders = getOrders;
        vm.getOrderDetail = getOrderDetail;
        vm.getFriends = getFriends;
        vm.getTotalPrice = getTotalPrice;
        vm.changePrice = changePrice;
        vm.placeOrder = placeOrder;
        vm.deleteOrder = deleteOrder;
        vm.deleteOrderFromCart = deleteOrderFromCart;
        vm.deleteOrderDetail = deleteOrderDetail;
        vm.saveChanges = saveChanges;
        vm.getSelectedFrnd = getSelectedFrnd;
        vm.changeSideDish = changeSideDish;
        vm.changeSideDishFromSelect = changeSideDishFromSelect;
        vm.closeAlert = closeAlert;
        vm.orders = [];
        vm.order = null;
        vm.orderDetail = [];
        vm.isOrderDetailEmpty = false;
        vm.isOrderEmpty = false;
        vm.friends = [];
        vm.selectedFriend = null;
        vm.totalPrice = 0;
        vm.orderSuccess = false;
        vm.orderError = false;
        vm.errorMessage = '';
        vm.successMessage = '';
        vm.restaurantId = $state.params.restaurantId;
        vm.loading = false;

        vm.currentClientId = 0;
        $translate(['DATE_PICKER.NOW', 'DATE_PICKER.CLEAR', 'DATE_PICKER.CLOSE', 'DATE_PICKER.DATE', 'DATE_PICKER.TIME', 'DATE_PICKER.TODAY'])
            .then(function(translations){
            uiDatetimePickerConfig.buttonBar.now.text = translations["DATE_PICKER.NOW"];
            uiDatetimePickerConfig.buttonBar.clear.text = translations["DATE_PICKER.CLEAR"];
            uiDatetimePickerConfig.buttonBar.close.text = translations["DATE_PICKER.CLOSE"];
            uiDatetimePickerConfig.buttonBar.date.text = translations["DATE_PICKER.DATE"];
            uiDatetimePickerConfig.buttonBar.time.text = translations["DATE_PICKER.TIME"];
            uiDatetimePickerConfig.buttonBar.today.text = translations["DATE_PICKER.TODAY"];
        }, function(translationId){
            debugger;
            uiDatetimePickerConfig.buttonBar.now.text = translationId["DATE_PICKER.NOW"];
            uiDatetimePickerConfig.buttonBar.clear.text = translationId["DATE_PICKER.CLEAR"];
            uiDatetimePickerConfig.buttonBar.close.text = translationId["DATE_PICKER.CLOSE"];
            uiDatetimePickerConfig.buttonBar.date.text = translationId["DATE_PICKER.DATE"];
            uiDatetimePickerConfig.buttonBar.time.text = translationId["DATE_PICKER.TIME"];
            uiDatetimePickerConfig.buttonBar.today.text = translationId["DATE_PICKER.TODAY"];
        });
        $rootScope.$on('$translateChangeSuccess', function(){
            $translate(['DATE_PICKER.NOW', 'DATE_PICKER.CLEAR', 'DATE_PICKER.CLOSE', 'DATE_PICKER.DATE', 'DATE_PICKER.TIME', 'DATE_PICKER.TODAY']).then(function(translations){
                debugger;
                uiDatetimePickerConfig.buttonBar.now.text = translations["DATE_PICKER.NOW"];
                uiDatetimePickerConfig.buttonBar.clear.text = translations["DATE_PICKER.CLEAR"];
                uiDatetimePickerConfig.buttonBar.close.text = translations["DATE_PICKER.CLOSE"];
                uiDatetimePickerConfig.buttonBar.date.text = translations["DATE_PICKER.DATE"];
                uiDatetimePickerConfig.buttonBar.time.text = translations["DATE_PICKER.TIME"];
                uiDatetimePickerConfig.buttonBar.today.text = translations["DATE_PICKER.TODAY"];
            }, function(translationId){
                debugger;
                uiDatetimePickerConfig.buttonBar.now.text = translationId["DATE_PICKER.NOW"];
                uiDatetimePickerConfig.buttonBar.clear.text = translationId["DATE_PICKER.CLEAR"];
                uiDatetimePickerConfig.buttonBar.close.text = translationId["DATE_PICKER.CLOSE"];
                uiDatetimePickerConfig.buttonBar.date.text = translationId["DATE_PICKER.DATE"];
                uiDatetimePickerConfig.buttonBar.time.text = translationId["DATE_PICKER.TIME"];
                uiDatetimePickerConfig.buttonBar.today.text = translationId["DATE_PICKER.TODAY"];
            });
        });


        // load order details if state is main.clientCart
        if ($state.current.name == "main.clientCart"){
            getOrderDetail($state.params.restaurantId);
        }

        getOrders();
        getFriends();
        getTotalPrice();

        vm.date_picker = {
            date: new Date('d.m.y H:i'),
            datepickerOptions: {
                showWeeks: false,
                minDate: new Date(),
                startingDay: 1
            }
        };

        vm.openCalendar = function(e, picker) {
            vm.orderDetail[picker].date_picker = true;

        };

        function getOrders(){
            vm.loading = true;
            ClientCartService.getOrders().then(function (response) {
                debugger;
                if(response.data && response.data.length == 0){
                    vm.isOrderEmpty = true;
                } else {
                    var orders = response.data;
                    vm.orders = [];
                    angular.forEach(orders,function(order){
                        if(order.total_order_details != 0){
                            vm.orders.push(order);
                        }
                    });
                    if (vm.orders.length == 1){
                        $state.go("main.clientCart", {restaurantId: vm.orders[0].ID_restaurant});
                    }
                }
                vm.loading = false;
            }, function (error) {
                debugger;
                vm.loading = true;
            });
        }

        function addNewKeys(orders) {
            debugger;
            angular.forEach(orders, function (value) {
                value['visible'] = true;
                value['date_picker'] = false;
                value['serve_at'] = value['serve_at'] == "30.11.-0001 00:00" ? new Date() : moment(value['serve_at'], "DD.MM.YYYY HH:mm").toDate();
                if (value.menu_list.data.photo){
                    value['photo'] = appConstant.imagePath + value.menu_list.data.photo;
                } else {
                    value['photo'] = "assets/images/meal-placeholder.png";
                }
                debugger;

                value['side_dish_bool'] = value['side_dish'] != '0';
                value['is_child'] = value['is_child'];
                value['friends'] = vm.friends;
                if (value.is_child){
                    value['t_price'] = (value.menu_list.data.price_child ? value.menu_list.data.price_child :value.menu_list.data.price ) * value.x_number;
                } else {
                    value['t_price'] = value.menu_list.data.price * value.x_number;
                }
            });
        }

        function getFriends() {
            ClientService.getFriendCircle().then(function (response) {
                debugger;
                if(!response.error) {
                    vm.friends = response.data;
                }
            }, function (error) {
                debugger;
            });
        }

        function getTotalPrice() {
            vm.totalPrice = 0;
            angular.forEach(vm.orderDetail, function (value, key) {
                if (value.is_child){
                    vm.totalPrice += (value.menu_list.data.price_child ? value.menu_list.data.price_child : value.menu_list.data.price) * value.x_number;
                } else {
                    vm.totalPrice += value.menu_list.data.price * value.x_number;
                }

            });
            vm.totalPrice = vm.totalPrice.toFixed(2);
        }

        function changePrice(order) {
            debugger;
            if (order.is_child){
                order.t_price = (order.menu_list.data.price_child ? order.menu_list.data.price_child :order.menu_list.data.price ) * order.x_number;
            } else {
                order.t_price = order.menu_list.data.price * order.x_number;
            }

            getTotalPrice();
        }

        function getOrder(orderId) {
            debugger;
            ClientService.getOrder(orderId).then(function (response) {
                debugger;
                vm.order = response.data;
                vm.currentClientId = response.data.ID_client;
            }, function (error) {
                debugger;
            })
        }

        function getOrderDetail(restaurantId) {
            vm.loading = true;
            ClientCartService.getOrderDetail(restaurantId).then(function (response) {
                debugger;
                if (response.error || (response.data && response.data.length == 0)) {
                    vm.isOrderDetailEmpty = true;
                } else {
                    vm.orderDetail = response.data;
                    debugger;
                    addNewKeys(vm.orderDetail);
                    getTotalPrice();
                    getOrder(vm.orderDetail[0].ID_orders);
                }

                vm.loading = false;
            }, function (error) {
                debugger;
                vm.loading = true;
            });
        }

        function placeOrder(isValid) {
            if (isValid){
                debugger;
                vm.loading = true;
                var order = {
                    "order": vm.order,
                    "orderDetail" : vm.orderDetail
                };

                ClientCartService.placeOrder(order).then(function (response) {
                    debugger;
                    if (response.data){
                        vm.orderSuccess = "CLIENT.ORDER SUCCESS MSG";
                        vm.orderError = "";
                        vm.orderDetail = [];
                        $rootScope.$broadcast('orders-detail-changed');
                        $timeout(function(){
                            vm.orderSuccess = "";
                            $state.go('main.clientDashboard');
                        }, 4000);
                    }
                    else if(response.wrongServingTime) {
                        vm.orderError = "CLIENT.WRONG SERVING TIME";
                        vm.wrongServing = response.wrongServingTime;
                        // $timeout(function(){
                        //     vm.orderError = "";
                        // }, 5000);
                    } else if (response.requestError){
                        vm.orderError = "CLIENT.ORDER ERROR";
                        // $timeout(function(){
                        //     vm.orderError = "";
                        // }, 3500);
                    }
                    vm.getOrderDetail($state.params.restaurantId);
                    vm.loading = false;
                }, function (error) {
                    debugger;
                    vm.orderError = "CLIENT.ORDER ERROR";
                    $rootScope.$broadcast('orders-detail-changed');
                    getOrderDetail($state.params.restaurantId)
                    $timeout(function(){
                        vm.orderError = "";
                    }, 3500);
                    vm.loading = false;
                })
            }
        }

        function closeAlert(ers) {
            debugger;
            if (ers == "error") {
                vm.orderError = "";
            } else {
                vm.orderSuccess = "";
            }
        }

        function deleteOrder(order) {
            debugger;
            ClientCartService.deleteOrder(order.ID_orders).then(function (response) {
                $rootScope.$broadcast('orders-detail-changed');
                vm.getOrders()
            }, function (error) {
                debugger;
            });
        }
        function deleteOrderFromCart(order) {
            debugger;
            ClientCartService.deleteOrder(order.ID_orders).then(function (response) {
                $rootScope.$broadcast('orders-detail-changed');
                $state.go("main.clientOrders");
            }, function (error) {
                debugger;
            });
        }

        function deleteOrderDetail(orderDetail) {
            debugger;
            ClientCartService.deleteOrderDetail(orderDetail.ID_orders_detail).then(function (response) {
                $rootScope.$broadcast('orders-detail-changed');

                vm.getOrderDetail(orderDetail.menu_list.data.ID_restaurant);
            }, function (error) {
                debugger;
            });
        }

        function saveChanges(isValid, changeState) {
            debugger;

            if (isValid){
                vm.loading = true;
                var order = {
                    "order": vm.order
                };

                var orders_detail = {
                    "orders_detail": vm.orderDetail

                };
                var resp = 0;
                debugger;
                ClientService.updateOrder(order).then(function (response) {
                    debugger;
                    getOrder(vm.orderDetail[0].ID_orders);
                    if (resp){
                        vm.loading = false;
                    } else {
                        resp = 1;
                    }
                    if (changeState){
                        angular.forEach(vm.orderDetail, function(item){
                            debugger;
                            item.date_picker = false;
                        });
                        debugger;
                        $state.go("main.restaurant_detail", { "restaurantId" : vm.restaurantId})
                    }
                    vm.orderSuccess = "CLIENT.ORDER SAVE SUCCESS MSG";
                    $timeout(function(){
                        vm.orderSuccess = "";
                    }, 3000);
                }, function (error) {
                    if (resp){
                        vm.loading = false;
                    } else {
                        resp = 1;
                    }
                });

                ClientService.updateOrderDetails(orders_detail).then(function (response) {
                    debugger;
                    vm.getOrderDetail(vm.restaurantId);
                    if (resp){
                        vm.loading = false;
                    } else {
                        resp = 1;
                    }
                }, function (error) {
                    debugger;
                    if (resp){
                        vm.loading = false;
                    } else {
                        resp = 1;
                    }
                })
            }


        }

        function getSelectedFrnd(friend, order) {
            if (friend.ID_client == order.ID_client) {
                
            }
        }

        function changeSideDish(order) {
            if (!order.side_dish_bool) {
                order.side_dish = 0;
            }
        }
        function changeSideDishFromSelect(order) {
            if (order.side_dish){
                angular.forEach(vm.orderDetail, function(order_detail){
                    debugger;
                    if ((order_detail.ID_orders_detail != order.ID_orders_detail) && order_detail.side_dish == order.ID_orders_detail){
                        debugger;
                        order_detail.side_dish = 0;
                        order_detail.side_dish_bool = false;
                    }
                });
            }

        }
    }
})();