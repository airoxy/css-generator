/**
 * Created by yonatom on 8/31/16.
 */

(function () {
    'use strict';

    angular
        .module('app.client')
        .service('ClientService', ClientService);
    /*@ngNoInject*/
    function ClientService(TokenRestangular, $rootScope, $state) {
        var service = {
            getClientDetail: getClientDetail,
            getFriends : getFriends,
            getFriendCircle: getFriendCircle,
            getOtherCircles: getOtherCircles,
            addFriends: addFriends,
            respondToFriendRequest: respondToFriendRequest,
            getFriendRequests: getFriendRequests,
            getSentFriendRequests : getSentFriendRequest,
            getOrders: getOrders,
            getOrder: getOrder,
            updateOrderDetails: updateOrderDetails,
            updateOrder: updateOrder

        };
        return service;

        function getClientDetail (clientId){
            debugger;
            return TokenRestangular.all('client/'+clientId).customGET('');
        }

        function getFriends(query){
            debugger;
            return TokenRestangular.all('clients?search='+query).customGET('');
        }
        function getFriendCircle(){
            debugger;
            return TokenRestangular.all('client/friends').customGET('');
        }
        function getOtherCircles(){
            debugger;
            return TokenRestangular.all('client/circles').customGET('');
        }
        function addFriends(clients){
            debugger;
            return TokenRestangular.all('client/friends').customPOST(clients);
        }
        function respondToFriendRequest(response){
            debugger;
            return TokenRestangular.all('client/respond').customPOST(response);
        }
        function getFriendRequests(){
            debugger;
            return TokenRestangular.all('client/requests').customGET('');
        }
        function getSentFriendRequest(){
            debugger;
            return TokenRestangular.all('client/sent_requests').customGET('');
        }
        function getOrders(currentPage) {
            debugger;
            return TokenRestangular.all('orders_by_status?page='+ currentPage).customGET('');
        }

        function getOrder(orderId) {
            debugger;
            return TokenRestangular.all('orders/' + orderId).customGET('');
        }
        
        function updateOrderDetails(orderDetails, source) {
            debugger;
            if (!source){
                angular.forEach(orderDetails.orders_detail, function(item){
                    item.serve_at = moment(item.serve_at).format();
                    debugger;
                });
            }
            return TokenRestangular.all('orders_detail').customPUT(orderDetails);
        }

        function updateOrder(order) {
            debugger;
            return TokenRestangular.all('orders').customPUT(order);
        }

    }

})();
