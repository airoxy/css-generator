<?php
/**
 * Created by PhpStorm.
 * RestaurantOpen: tOm_HydRa
 * Date: 9/10/16
 * Time: 12:06 PM
 */

namespace App\Repositories;

use App\Entities\Client;
use App\Entities\MenuList;
use App\Entities\Order;
use App\Entities\OrderDetail;
use App\Entities\RestaurantOpen;
use App\Entities\Restaurant;
use App\User;
use Carbon\Carbon;
use Faker\Provider\zh_TW\DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Webpatser\Uuid\Uuid;


class OrderDetailRepository
{
    private $initialTime = "0001-01-01 00:00:00";
    public $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;

    }

    public function store(Request $request){
        if ($request->has("orders_detail")){
            $client = app('Dingo\Api\Auth\Auth')->user()->client;
            $orders_detail = OrderDetail::where(["ID_menu_list" => $request->orders_detail["ID_menu_list"], "ID_client" => $client->ID, "status" => 5]);
            if ($orders_detail->count()){
                $orders_detail = $orders_detail->first();
                $orders_detail->x_number += 1;
                $orders_detail->save();
                return $orders_detail;
            }
            $input = $request->orders_detail;
            $menu_list = MenuList::find($input["ID_menu_list"]);
            $orders_detail = new OrderDetail();
            $order = $this->orderRepository->save($menu_list->restaurant->id);
            $orders_detail->ID_orders = $order->ID ? $order->ID : $order->id;
            $orders_detail->ID_client = $client->ID;
            $orders_detail->ID_menu_list = $input["ID_menu_list"];
            $orders_detail->is_child = 0;
            $orders_detail->price = $menu_list->price;
            $orders_detail->side_dish = 0;
            $orders_detail->status = 5;
            $orders_detail->x_number = 1;
            $orders_detail->save();
            return $orders_detail;
        }
    }

    public function update($input){
        $orders_detail = OrderDetail::find($input["ID_orders_detail"]);
        $orders_detail->ID_client = isset($input["ID_client"]) ? $input["ID_client"] : $orders_detail->ID_client;
        $orders_detail->x_number = isset($input["x_number"]) ? $input["x_number"] : $orders_detail->x_number;
        $orders_detail->comment = isset($input["comment"]) ? $input["comment"] : $orders_detail->comment;
        $orders_detail->is_child = isset($input["is_child"]) ? $input["is_child"] : $orders_detail->is_child;
        $orders_detail->price = isset($input["t_price"]) ? $input["t_price"] : $orders_detail->price;
        $orders_detail->side_dish = isset($input["side_dish"]) ? $input["side_dish"] : $orders_detail->side_dish;
        $orders_detail->serve_at = isset($input["serve_at"]) ? $input["serve_at"] : new \DateTime();
        $orders_detail->status = 0;
        $orders_detail->save();
        $order = Order::find($orders_detail->order->ID);
        $order->status = 0;
        $order->save();
        return $orders_detail;

    }

    public function respond($input){
        $order = $input->order;
        foreach ($order as $orders_detail) {
            $this->update($orders_detail);
        }
        $order_email = Order::find($order[0]['ID_orders']);
        $sent = $this->sendEmailReminder('new', app('Dingo\Api\Auth\Auth')->user(), $order_email,$order_email->restaurant,
            $input->lang ? $input->lang: 'cz', $order, 'user');
        $sent_rest = $this->sendEmailReminder('new', app('Dingo\Api\Auth\Auth')->user(), $order_email, $order_email->restaurant,
            'cz', $order, 'rest');
        return $order;
    }

    public function all($restaurantId){
        $client = app('Dingo\Api\Auth\Auth')->user()->client;
        $order = Order::where(["ID_client" => $client->ID, "ID_restaurant" => $restaurantId, "status" => 5])->first();
        if ($order){
            $orders_detail =  $order->orders_detail->filter(function($item){
               if ($item->status == 5){
                   if ($item->side_dish){
                       $item->order_by_side_dish = $item->side_dish + .1;
                       $item->serve_at_sort = OrderDetail::find($item->side_dish)->serve_at_sort;
                   } else {
                       $item->order_by_side_dish = $item->ID;
                       $item->serve_at_sort = $item->serve_at;
                   }
                   return true;

               }
               return false;
            });
            $orders_detail = $orders_detail->sortBy(function($order_detail) {
                return sprintf('%-12s%s', $order_detail->order_by_side_dish, $order_detail->serve_at);
            });
            return $orders_detail;
        }
        return false;
    }

    public function getOrders()
    {
        $client = app('Dingo\Api\Auth\Auth')->user()->client;
        $order = Order::where(["ID_client" => $client->ID, "status" => 5])->get();
        if ($order){
            return $order;
        }
        return false;

    }

    public function getOrdersDetailByStatus($status, $orderId, $n)
    {
        $orders_detail = OrderDetail::where(["ID_orders" => $orderId, "status" => $status])->paginate($n);
        if ($orders_detail){
            return $orders_detail;
        }
        return false;
    }

    public function getOrdersByStatus($status, $n)
    {
        $client = app('Dingo\Api\Auth\Auth')->user()->client;
        $orders = Order::where(["ID_client" => $client->ID, "status" => $status])->paginate($n);
        if ($orders){
            return $orders;
        }
        return false;
    }

    public function getAllOrders($n)
    {
        $client = app('Dingo\Api\Auth\Auth')->user()->client;
        $orders = Order::where(["ID_client" => $client->ID])->paginate($n);
        if ($orders){
            return $orders;
        }
        return false;
    }

    public function deleteOrder($orderId){
        $order = Order::find($orderId);
        if ($order){
            $order_details = $order->orders_detail;
            foreach ($order_details as $order_detail) {
                $ord_detail = OrderDetail::find($order_detail->ID);
                $ord_detail->status = 3;
                $ord_detail->save();
            }
            $order->status = 3;
            $order->save();
            return $order;
        }
        return false;
    }

    public function getOrder($orderId)
    {
        $order = Order::find($orderId);
        $orders_detail = $order->orders_detail;
        $orders_detail = $orders_detail->filter(function($item){
            if ($item->side_dish){
                $item->order_by_side_dish = $item->side_dish + .1;
                $item->serve_at_sort = OrderDetail::find($item->side_dish)->serve_at_sort;
            } else {
                $item->order_by_side_dish = $item->ID;
                $item->serve_at_sort = $item->serve_at;
            }
            return true;
        });
        $order->orders_detail = $orders_detail->sortBy(function($order_detail) {
//            return sprintf('%-12s%s', $order_detail->serve_at_sort, $order_detail->order_by_side_dish);
            return sprintf('%-12s%s', $order_detail->order_by_side_dish, $order_detail->serve_at);
        });

        if ($order) {
            return $order;
        }
        return false;
    }

    public function deleteOrderDetail($orderDetailId){
        $orderDetail = OrderDetail::find($orderDetailId);
        if ($orderDetail){
            $orderDetail->status = 3;
            $orderDetail->save();
            if ($orderDetail->sideDish && count($orderDetail->sideDish)){
                foreach ($orderDetail->sideDish as $item) {
                    $item->status = 3;
                    $item->save();
                }
            }
            return $orderDetail;
        }
        return false;
    }

    public function canCancel($menuList, $orderDetail){
        $initial_time = Carbon::instance(new \DateTime($this->initialTime));
        $cancel_until = Carbon::instance(new \DateTime($menuList->cancel_until));

        $serve_at = Carbon::instance(new \DateTime($orderDetail->serve_at));
        $current_time = Carbon::now();

        if ($current_time->gte($serve_at)){
            return false;
        }

        $diff_serve_at_and_current_time = $current_time->diffInMinutes($serve_at);
        $diff_cancel_until_and_initial_time = $cancel_until->diffInMinutes($initial_time);

        if ($diff_serve_at_and_current_time > $diff_cancel_until_and_initial_time){
            return false;
        }

    }

    public function sendEmailReminder($type, User $user, Order $order, $restaurant, $lang, $orders_detail, $to)
    {
        $path = 'emails.order.order_'.$type. '_'.$lang;
        return Mail::send($path,
            ['user' => $user, 'order' => $order, 'restaurant'=> $restaurant,
                'orders_detail_count'=> count($order->orders_detail),
                'orders_detail_total_price' => $this->getTotalPrice($order->orders_detail)],
            function ($m) use($user, $restaurant, $to){
                $m->from('cesko@gastro-booking.com', "Gastro Booking");
                if ($to == 'user')
                    $m->to($user->email, $user->name)->subject('Gastro Booking - Order');
                if ($to == 'rest')
                    $m->to($restaurant->email, $restaurant->name)->subject('Gastro Booking - Order');
        });
    }

    private function getTotalPrice($orders_detail)
    {
        $price = 0;
        foreach ($orders_detail as $order_detail) {
            if ($order_detail->is_child){
                $price += ($order_detail->menu_list->price_child ? $order_detail->menu_list->price_child : $order_detail->menu_list->price);
            } else {
                $price += $order_detail->menu_list->price;
            }
        }
        return $price;
    }

    public function getOrderDetailCount()
    {
        $user = app('Dingo\Api\Auth\Auth')->user();
        if (!$user){
            return 0;
        }
        $client = $user->client;
        $order_detail_count = 0;
        $order_total = Order::where(["ID_client" => $client->ID, "status" => 5])->get();
        if (count($order_total) == 1){
            $order_detail_count = OrderDetail::where(["ID_orders" => $order_total[0]->ID, "status" => 5])->count();
        } else if (count($order_total) > 1){
            $order_detail_count = count($order_total);
        }
        return $order_detail_count;
    }



}