<?php

class JSGenerator {
	private $scripts = [];
	private $scripts_string = null;

	/**
	 * Function for adding scripts into `JSGenerator` class
	 *
	 * @param $selectors Array - Parameters for adding to `$this->selectors` variable
	 * @return $this
	 */
	public function setScripts($scripts) {
		$this->scripts = array_merge($this->scripts, $scripts);

		return $this;
	}

	/**
	 * Function for converting array with scripts into scripts string
	 *
	 * @param $params Array - Parameters with elements and his css selectors
	 * @return $this
	 */
	public function generate($params) {
		foreach($params as $key => $val) {
			if(in_array($key, array_keys($this->scripts))) {
				$this->scripts_string .= $this->scripts[$key];

				// Add to script variable
				$this->scripts_string = str_replace("@{" . $key . "}", $val, $this->scripts_string);
			}
		}

		// Add init function jQuery
		$this->scripts_string= "$(function(){ setTimeout(function() {" . $this->scripts_string . "}, 100); });";

		return $this;
	}

	/**
	 * Function for getting js scripts an string
	 *
	 * @return string
	 */
	public function get() {
		return $this->scripts_string;
	}
}
