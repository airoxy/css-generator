<?php

class CSSGenerator {
	private $selectors = [];
	private $styles = [];
	private $styles_string = null;

	/**
	 * Function for adding parameters into `CSSGenerator` class
	 *
	 * @param $selectors Array - Parameters for adding to `$this->selectors` variable
	 * @return $this
	 */
	public function setSelectors($selectors) {
		$this->selectors = array_merge($this->selectors, $selectors);

		return $this;
	}

	/**
	 * Function for converting array with parameters into css array
	 *
	 * @param $params Array - Parameters with elements and his css selectors
	 * @return $this
	 */
	public function generate($params) {
		$styles = [];

		foreach($params as $key_parameter => $parameters) {
			if(in_array($key_parameter, array_keys($this->selectors))) {
				$selector = $this->selectors[$key_parameter];

				// when selector with parameter
				if(is_array($this->selectors[$key_parameter])) {
					$parameter = $parameters;

					$options = isset($selector[2]) ? $selector[2] : null;
					if($options && is_array($options) && isset($options[$parameter])) {
						$parameter = $options[$parameter];
					}

					$styles[] = $this->generateSelectors($key_parameter, $selector[0], [$selector[1] => $parameter]);
				}
				// when selector without parameter
				else {
					$styles[] = $this->generateSelectors($key_parameter, $selector, $parameters);
				}
			}
		}

		$this->styles = $styles;

		return $this;
	}

	/**
	 * Function for generate CSS selector an array
	 *
	 * @param $key_parameter - Key name for call need selector from `$this->selectors`
	 * @param $selector - Css selector
	 * @param $parameters - Parameters current `$selector`
	 * @return mixed Array - Array with elements of selector
	 */
	private function generateSelectors($key_parameter, $selector, $parameters) {
		$styles[$key_parameter][] = "{$selector} {";
		foreach($parameters as $key => $parameter) {
			$styles[$key_parameter][] = "{$key}: {$parameter} !important;";
		}
		$styles[$key_parameter][] = "}";

		return $styles;
	}

	/**
	 * Function for converting CSS array with selectors in string
	 *
	 * @return $this
	 */
	public function getSelectors() {
		$styles = null;
		foreach ($this->styles as $type_styles) {
			foreach ($type_styles as $selectors) {
				foreach ($selectors as $key => $selector) {
					// Added tab for all attributes in selector
					$tab = !($key == 0 || $key == count($selectors) - 1) ? "\t" : null;
					$styles .= $tab. $selector . "\n";
				}
				$styles .= "\n";
			}
		}

		$this->styles_string = $styles;

		return $this;
	}

	/**
	 * Function that adding tags for styles
	 *
	 * @return string
	 */
	public function getCSS() {
		$styles_string = $this->styles_string;
		return "<style type=\"text/css\">{$styles_string}</style>";
	}

	/**
	 * Function for getting css selectors an string
	 *
	 * @return string
	 */
	public function get() {
		return $this->styles_string;
	}
}
